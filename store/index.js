export const state = () => ({
  lst: [],
  trimmedList: [],
  filteredList: [],
  filteredShortList: [],
  filtered: false
});

export const mutations = {
  setData(state, lst) {
    state.lst = lst;
  },
  setTrimmedList(state, trimmedList) {
    state.trimmedList = trimmedList;
  },
  setFilteredList(state, payload) {
    state.filteredList = payload[0];
    state.filteredShortList = payload[1];
  },
  changeFiltered(state, value) {
    state.filtered = value;
  }
};

export const actions = {
  /**
   * Получает данные с бека и записывает в стор
   */
  async getData(context) {
    const lst = await fetch(
      "https://recruting-test-api.herokuapp.com/api/v1/brands"
    ).then(data => data.json());
    context.commit("setData", lst);
    await context.dispatch("sortData");
    await context.dispatch("trimList");
  },
  /**
   * Сортирует данные
   */
  async sortData(context) {
    let rawList = this.state.lst;
    rawList = rawList.reduce((sorted, elem) => {
      let group = elem.title[0].toLowerCase();
      if (!sorted[group]) sorted[group] = { elements: [elem] };
      else sorted[group].elements.push(elem);
      return sorted;
    }, {});
    const sortedList = [];
    for (let i in rawList) {
      sortedList.push(
        Object.values(rawList[i].elements).sort((a, b) =>
          a.title.localeCompare(b.title)
        )
      );
    }
    context.commit("setData", sortedList);
  },
  /**
   * Обрезает данные для отображения в коротком виде
   */
  async trimList(context) {
    let trimmedList = this.state.lst.map(x =>
      x.filter(x => x.main).slice(0, 5)
    );
    trimmedList = trimmedList.map((x, index) =>
      x.length > 0 ? x : this.state.lst[index].slice(0, 5)
    );
    context.commit("setTrimmedList", trimmedList);
  },
  /**
   * Сортирует данные
   */
  filterList(context, payload) {
    context.commit("changeFiltered", true);
    let lists = [this.state.lst, this.state.trimmedList];
    lists = lists.map(x =>
      x.map(x =>
        x.filter(x =>
          payload.lowerC
            ? x.title.includes(payload.pattern)
            : x.title.toLowerCase().includes(payload.pattern.toLowerCase())
        )
      )
    );
    const newLists = [[], []];
    for (let i = 0; i < lists[0].length; i++) {
      for (let j = 0; j < lists[1].length; j++) {
        if (lists[0][i].length + lists[1][j].length > 0 && i === j) {
          newLists[0].push(lists[0][i]);
          newLists[1].push(lists[1][j]);
        }
      }
    }
    context.commit("setFilteredList", { ...newLists });
  }
};

export const getters = {
  getData: state => {
    return state.lst;
  },
  getTrimmedList: state => {
    return state.trimmedList;
  },
  getFilteredList: state => {
    return state.filteredList;
  },
  getFilteredShortList: state => {
    return state.filteredShortList;
  },
  getFilteredState: state => {
    return state.filtered;
  }
};
